import Validity._
import io.circe.literal._
import org.scalatest._

class ValidityTest extends FlatSpec with Matchers {
  val noValid = json"""{ "timestamp": 1535803200,"temperature": 32}"""
  val negValid = json"""{ "timestamp": 1535803200, "validity": -1,"temperature": 32}"""
  val properValid = json"""{ "timestamp": 1535803200, "validity": 1535803260,"temperature": 32}"""

  val time = 1535803200
  val default = 50

  "Validity field" should "get corrected when needed" in {
    correct(default)(noValid).flatMap(_.hcursor.get[Int]("validity")) shouldBe Right(time + default)
    correct(default)(negValid).flatMap(_.hcursor.get[Int]("validity")) shouldBe Right(time + default)
    correct(default)(properValid) shouldBe Right(properValid)
  }
}
