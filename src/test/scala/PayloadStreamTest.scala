import io.circe.Json
import io.circe.syntax._
import org.scalatest._

import scala.collection.immutable

class PayloadStreamTest extends FlatSpec with Matchers {
  val packets: immutable.IndexedSeq[TimeSourcePayload] =
    for (i <- 1 to 10)
      yield TimeSourcePayload(timestamp = i, validity = i + 30, source = s"tag${i % 3}", payload = Map("v" -> i).asJson)

  val m: Json = PayloadStream.mergePayloads(packets)

  "Merging" should "produce valid timestamps" in {
    m.hcursor.get[Int]("timestamp") shouldBe Right(packets.map(_.timestamp).max)
    m.hcursor.get[Int]("validity") shouldBe Right(packets.map(_.validity).min)
  }

  it should "produce a valid payload" in {
    val packetVs = packets.map(_.payload.hcursor.get[Int]("v")).collect { case Right(v) => v }.toSet
    m.hcursor.get[Int]("v").map(packetVs.contains) shouldBe Right(true)
  }
}

