import TimeSourcePayload._
import io.circe.literal._
import org.scalatest.{FlatSpec, Matchers}

class TimeSourcePayloadTest extends FlatSpec with Matchers {
  val json = json"""{ "timestamp": 1535803200,"validity": 1535806800,"smallVehicles": 15.5, "largeVehicles": 2.3}"""
  val source = "Source!"

  "Json" should "get wrapped" in {
    wrap(source)(json) shouldBe Right(TimeSourcePayload(1535803200, 1535806800, source, json))
  }
}
