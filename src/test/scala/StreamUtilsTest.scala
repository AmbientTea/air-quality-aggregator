import StreamUtils._
import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.{Keep, Sink, Source}
import akka.util.ByteString
import org.scalatest._

import scala.collection.immutable._
import scala.concurrent.Await
import scala.concurrent.duration._

class StreamUtilsTest extends FlatSpec with Matchers {
  implicit val actorSystem: ActorSystem = ActorSystem("test")
  implicit val materializer: ActorMaterializer = ActorMaterializer()

  "mergeSorted" should "produce sorted stream with the same elements" in {
    val packets = 1 to 100
    val packetSources = packets.grouped(20).map(Source[Int]).toIterable
    val mergedSource = mergeSortedN(packetSources)
    val pipe = mergedSource.toMat(Sink.seq)(Keep.right)
    val result = Await.result(pipe.run(), 3.seconds).toStream

    def isSorted(l: Iterable[Int]) = l.drop(1).zip(l.dropRight(1)).forall { p => p._1 > p._2 }

    result.size shouldBe packets.size
    result.toSet shouldBe packets.toSet
    isSorted(result) shouldBe true
  }

  "lines" should "split lines" in {
    val lines = for (i <- 1 to 10) yield i.toString * 10
    val byteStrings = lines.mkString("\n").grouped(4).map(ByteString(_)).to[Iterable]
    val byteStream = Source.apply[ByteString](byteStrings)

    val result = Await.result(byteStream.via(StreamUtils.lines).runWith(Sink.seq), 3.seconds)

    result shouldBe lines
  }
}
