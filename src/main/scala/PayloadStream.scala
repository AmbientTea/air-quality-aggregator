import akka.NotUsed
import akka.stream.scaladsl.Flow
import io.circe.Json
import io.circe.syntax._

object PayloadStream {
  // for each packet emits the set of all valid at this packets,
  val activeBySource: Flow[TimeSourcePayload, Iterable[TimeSourcePayload], NotUsed] =
    Flow[TimeSourcePayload]
      .scan(Map[String, TimeSourcePayload]()) { (oldPayloads, newPayload) =>
        oldPayloads.filter(_._2.validity > newPayload.timestamp) + (newPayload.source -> newPayload)
      }
      .drop(1) // drop initial empty Map
      .map(_.values)

  // merges packet payloads into one json, with max timestamp and min validity
  def mergePayloads(packets: Iterable[TimeSourcePayload]): Json = {
    val validity = packets.map(_.validity).min
    val timestamp = packets.map(_.timestamp).max
    packets.map(_.payload)
      .reduce(_ deepMerge _)
      .deepMerge(Map("timestamp" -> timestamp, "validity" -> validity).asJson)
  }

  val mergePayloadsFlow: Flow[Iterable[TimeSourcePayload], Json, NotUsed] =
    Flow[Iterable[TimeSourcePayload]] map mergePayloads
}
