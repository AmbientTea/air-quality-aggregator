import io.circe._
import io.circe.syntax._

object Validity {
  def correct(defaultDuration: Int)(json: Json): Either[DecodingFailure, Json] = {
    val curs = json.hcursor
    for {
      start <- curs.get[Int]("timestamp")
      end = curs.get[Int]("validity").toOption.filter(_ > start).getOrElse(start + defaultDuration)
    } yield json.deepMerge(Map("validity" -> end).asJson)
  }
}
