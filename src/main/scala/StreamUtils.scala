import java.nio.file.Paths

import akka.event.Logging
import akka.stream.scaladsl.GraphDSL.Implicits._
import akka.stream.scaladsl._
import akka.stream.{Attributes, FlowShape, Graph, IOResult}
import akka.util.ByteString
import akka.{Done, NotUsed}

import scala.concurrent.Future

object StreamUtils {
  // merges and sorts any number of inputs
  def mergeSortedN[Out, Mat](inputs: Iterable[Source[Out, Mat]])(implicit ord: Ordering[Out]): Source[Out, Mat] =
    inputs.reduce(_ mergeSorted _)

  // converts ByteString flow to lines
  val lines: Flow[ByteString, String, NotUsed] =
    Flow[ByteString]
      .via(Framing.delimiter(ByteString("\n"), maximumFrameLength = 256, allowTruncation = true) // split byt stream to lines
        .map(_.utf8String))

  // filters out and logs left values in either stream, while passing along the right values
  def logLefts[Out, Err](logName: String, logLevel: Logging.LogLevel): Graph[FlowShape[Either[Err, Out], Out], NotUsed] =
    GraphDSL.create() { implicit b =>
      val bcast = b.add(Broadcast[Either[Err, Out]](outputPorts = 2))
      val leftPipe = Flow[Either[Err, Out]]
        .collect { case Left(v) => v }
        .log(logName)
        .addAttributes(Attributes.logLevels(onElement = logLevel))
        .to(Sink.ignore)
      val rightPipe = b.add(Flow[Either[Err, Out]].collect { case Right(v) => v })

      bcast ~> leftPipe
      bcast ~> rightPipe

      FlowShape(bcast.in, rightPipe.out)
    }

  // write each string as a separate line onto a file
  def sinkLinesToFile(fileName: String): Sink[String, Future[IOResult]] = {
    val fileSink = FileIO.toPath(Paths.get(fileName))
    Flow[String].mapConcat(s => List(ByteString(s), ByteString("\n"))).toMat(fileSink)(Keep.right)
  }

  // write each string as a separate line to stdout
  def sinkLinesToStdOut: Sink[String, Future[Done]] = Sink.foreach[String](println)
}
