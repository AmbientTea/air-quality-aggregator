import io.circe.{DecodingFailure, Json}

case class TimeSourcePayload(timestamp: Int, validity: Int, source: String, payload: Json)

object TimeSourcePayload {
  def wrap(source: String)(json: Json): Either[DecodingFailure, TimeSourcePayload] = {
    val curs = json.hcursor
    for {
      start <- curs.get[Int]("timestamp")
      end <- curs.get[Int]("validity")
    } yield TimeSourcePayload(start, end, source, json)
  }

  val orderByTimestamp: Ordering[TimeSourcePayload] = Ordering.by(_.timestamp)
}