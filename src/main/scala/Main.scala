import java.nio.file.{Path, Paths}

import TimeSourcePayload._
import akka.actor.ActorSystem
import akka.stream.scaladsl.{FileIO, Flow}
import akka.stream.{ActorMaterializer, Attributes}
import com.typesafe.config.ConfigFactory
import com.typesafe.scalalogging.LazyLogging
import io.circe.parser._

import scala.collection.JavaConverters._
import scala.concurrent.ExecutionContext.Implicits.global
import scala.util.{Failure, Success, Try}

object Main extends LazyLogging {
  def main(args: Array[String]): Unit = {
    val config = ConfigFactory.load()

    val inputs = config.getObject("inputs").unwrapped.asScala.collect { case p@(key, value: String) => p }.toMap

    val defaultDuration = config.getInt("validity.defaultDuration")

    logger.debug(s"Application started with def. duration = defaultDuration and inputs: $inputs")

    implicit val actorSystem: ActorSystem = ActorSystem("air-quality")
    implicit val materializer: ActorMaterializer = ActorMaterializer()


    val sources = for {
      (fileTag, fileName) <- inputs
      path: Path = Paths.get(fileName.toString)
    } yield FileIO.fromPath(path)
      .via(StreamUtils.lines)
      .map(parse)
      .map(_ flatMap Validity.correct(defaultDuration))
      .map(_ flatMap TimeSourcePayload.wrap(fileTag))
      .via(StreamUtils.logLefts("parsing", Attributes.LogLevels.Error))

    val sourcesMerge = StreamUtils.mergeSortedN(sources)(orderByTimestamp)

    val processing = Flow[TimeSourcePayload]
      .via(PayloadStream.activeBySource)
      .via(PayloadStream.mergePayloadsFlow)

    val output = Try(config.getString("output.file"))
      .map(StreamUtils.sinkLinesToFile)
      .getOrElse(StreamUtils.sinkLinesToStdOut)

    val result = sourcesMerge
      .via(processing)
      .map(_.noSpaces)
      .runWith(output)

    result.onComplete { res =>
      res match {
        case Success(value) =>
          logger.info("Application finished successfully")
        case Failure(ex) =>
          logger.error(s"Application finished with exception: ${ex.getMessage}")
      }
      actorSystem.terminate()
    }
  }
}
